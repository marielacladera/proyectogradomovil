﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveVideo : MonoBehaviour
{
    // Start is called before the first frame update
    public SimpleCloudRecoEventHandler instance;
    public int contador = 0;

    public void atras()
    {
        List<string> lista = instance.getVideos();
        int listSize = lista.Count;

        if (contador > 0)
        {
            var video = GameObject.Find("VideoPlayer").GetComponent<UnityEngine.Video.VideoPlayer>();
            string url = "https://firebasestorage.googleapis.com/v0/b/friendly-art-304619.appspot.com/o/" + lista[contador] + "?alt=media";

            video.url = url;
            video.playOnAwake = false;
            contador = contador - 1;

        }
    }

    public void adelante()
    {
        List<string> lista = instance.getVideos();
        int listSize = lista.Count;

        if (contador < listSize - 1) {
            var video = GameObject.Find("VideoPlayer").GetComponent<UnityEngine.Video.VideoPlayer>();
            string url = "https://firebasestorage.googleapis.com/v0/b/friendly-art-304619.appspot.com/o/" + lista[contador]+ "?alt=media";

            video.url = url;
            video.playOnAwake = false;
            contador = contador + 1;
        }
    }
}
