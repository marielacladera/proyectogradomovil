﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControladorEscenas : MonoBehaviour
{
    public void CargarNivel(string nombreEscena){
        SceneManager.LoadScene(nombreEscena);
    }

    public void SalirAplicacion(){
        Application.Quit();
    }

   public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("MenuPrincipal");
        }
    }
}
