﻿using System;
using System.Collections.Generic;
    public class CuriousQuestion
    {
        public string stringValue { get; set; }
    }

    public class Answer
    {
        public string stringValue { get; set; }
    }

    public class Filas
    {
        public CuriousQuestion curious_question { get; set; }
        public Files files { get; set; }
        public Answer answer { get; set; }
    }

    public class Document
    {
        public string name { get; set; }
        public Filas fields { get; set; }
        public DateTime createTime { get; set; }
        public DateTime updateTime { get; set; }
    }

    public class Root
    {
        public List<Document> documents { get; set; }
    }



