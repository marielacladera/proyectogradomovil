﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlay : MonoBehaviour
{
    private bool pause = true;

    public void settings()
    {
        var audio = GameObject.Find("Audio").GetComponent<UnityEngine.AudioSource>();
        if (pause == true)
        {
            audio.Pause();
            audio.playOnAwake = false;
            pause = false;
        }
        else
        {
            audio.Play();
            audio.playOnAwake = true;
            pause = true;
        }
    }
}
