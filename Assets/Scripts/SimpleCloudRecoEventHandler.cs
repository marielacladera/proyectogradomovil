﻿using System.Collections;
using System.Collections.Generic;
using Vuforia;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using Newtonsoft.Json;

public class SimpleCloudRecoEventHandler : MonoBehaviour
{
    private CloudRecoBehaviour mCloudRecoBehaviour;
    private bool mIsScanning = false;
    private string mTargetMetadata = "";
    private string targetId = "";
    private FirestoreResponse firestoreResponse;
    // Register cloud reco callbacks
    public ImageTargetBehaviour ImageTargetTemplate;
    public UnityEngine.UI.Image image;
    public List<string> videos = new List<string>();
    public List<string> imagenes = new List<string>();
    public List<string> audios = new List<string>();

    void Start() 
    {
       /* GameObject salir = GameObject.Find("Salir");
        GameObject volver = GameObject.Find("Volver");
        GameObject volverEscanear = GameObject.Find("VolverEscanear");

        salir.SetActive(true);
        volver.SetActive(true);
        volverEscanear.SetActive(true);*/

    }

    void Awake()
    {
        mCloudRecoBehaviour = GetComponent<CloudRecoBehaviour>();
        mCloudRecoBehaviour.RegisterOnInitializedEventHandler(OnInitialized);
        mCloudRecoBehaviour.RegisterOnInitErrorEventHandler(OnInitError);
        mCloudRecoBehaviour.RegisterOnUpdateErrorEventHandler(OnUpdateError);
        mCloudRecoBehaviour.RegisterOnStateChangedEventHandler(OnStateChanged);
        mCloudRecoBehaviour.RegisterOnNewSearchResultEventHandler(OnNewSearchResult);
    }
    //Unregister cloud reco callbacks when the handler is destroyed
    void OnDestroy()
    {
        mCloudRecoBehaviour.UnregisterOnInitializedEventHandler(OnInitialized);
        mCloudRecoBehaviour.UnregisterOnInitErrorEventHandler(OnInitError);
        mCloudRecoBehaviour.UnregisterOnUpdateErrorEventHandler(OnUpdateError);
        mCloudRecoBehaviour.UnregisterOnStateChangedEventHandler(OnStateChanged);
        mCloudRecoBehaviour.UnregisterOnNewSearchResultEventHandler(OnNewSearchResult);
    }
    public void OnInitialized(TargetFinder targetFinder)
    {
       
        Debug.Log("Cloud Reco initialized");
    }
    public void OnInitError(TargetFinder.InitState initError)
    {
        Debug.Log("Cloud Reco init error " + initError.ToString());
    }
    public void OnUpdateError(TargetFinder.UpdateState updateError)
    {
        Debug.Log("Cloud Reco update error " + updateError.ToString());
    }
    public void OnStateChanged(bool scanning)
    {
        mIsScanning = scanning;
        if (scanning)
        {
            // clear all known trackables

            var tracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
            tracker.GetTargetFinder<ImageTargetFinder>().ClearTrackables(false);
        }
    }
    // Here we handle a cloud target recognition event
    public void OnNewSearchResult(TargetFinder.TargetSearchResult targetSearchResult)
    {
        TargetFinder.CloudRecoSearchResult cloudRecoSearchResult = (TargetFinder.CloudRecoSearchResult)targetSearchResult;
        targetId = targetSearchResult.UniqueTargetId;
        // do something with the target metadata
        mTargetMetadata = cloudRecoSearchResult.MetaData;
        // stop the target finder (i.e. stop scanning the cloud)
        mCloudRecoBehaviour.CloudRecoEnabled = false;
        // Build augmentation based on target
        if (ImageTargetTemplate)
        {
            // enable the new result with the same ImageTargetBehaviour:
            GetData();
            ObjectTracker tracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
            tracker.GetTargetFinder<ImageTargetFinder>().EnableTracking(targetSearchResult, ImageTargetTemplate.gameObject);
        }
    }

    private void GetData()
    {
        StartCoroutine(getCourritine());
    }

    private IEnumerator getCourritine()
    {
        UnityWebRequest request = UnityWebRequest.Get("https://firestore.googleapis.com/v1/projects/friendly-art-304619/databases/(default)/documents/species/" + targetId);
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
        } else
        {
            var bodyString = request.downloadHandler.text;
            firestoreResponse = JsonConvert.DeserializeObject<FirestoreResponse>(bodyString);

            setUIText();
            setUIFiles();
        }
    }

    private void setUIText() 
    {
        Text nombreComun = GameObject.Find("NombreComun").GetComponent<Text>();
        Text nombreCientifico = GameObject.Find("NombreCientifico").GetComponent<Text>();
        Text era = GameObject.Find("Era").GetComponent<Text>();
        Text genero = GameObject.Find("Genero").GetComponent<Text>();
        Text habitad = GameObject.Find("Habitad").GetComponent<Text>();
        Text peso = GameObject.Find("Peso").GetComponent<Text>();
        Text alto = GameObject.Find("Alto").GetComponent<Text>();
        Text ancho = GameObject.Find("Ancho").GetComponent<Text>();
        Text descripcion = GameObject.Find("Descripcion").GetComponent<Text>();

        nombreComun.text = "Nombre comun : " + firestoreResponse.fields.common_name.stringValue;
        nombreCientifico.text = "Nombre cientifico : " + firestoreResponse.fields.scientific_name.stringValue;
        era.text = "Era : " + firestoreResponse.fields.was.stringValue;
        genero.text = "Genero : " + firestoreResponse.fields.gender.stringValue;
        habitad.text = "Habitad : " + firestoreResponse.fields.habitad.stringValue;
        peso.text = "Peso : " + firestoreResponse.fields.weight.stringValue;
        alto.text = "Alto : " + firestoreResponse.fields.high.stringValue;
        ancho.text = "Ancho : " + firestoreResponse.fields.width.stringValue;
        descripcion.text = "Descripcion : " + firestoreResponse.fields.description.stringValue;
    }

    private void setUIFiles() 
    {
        setFiles();
    }

    private void setFiles()
    {
        var links = firestoreResponse.fields.files.arrayValue.values;

        for (var i = 0; i < links.Count; i++)
        {
            string file = links[i].stringValue;
            string[] divs = file.Split('.');

            if (divs[1].ToLower().Equals("png") || divs[1].ToLower().Equals("jpg")) 
            {
                StartCoroutine(setUIImage(links[i].stringValue));
            }
            if (divs[1].ToLower().Equals("mp4"))
            {
                setUIVideo(links[i].stringValue);
            }
            if (divs[1].ToLower().Equals("mp3"))
            {
                StartCoroutine(setAudioClip(links[i].stringValue));
            }
            
        }
    }

    private IEnumerator setUIImage(string nameFile) {
        imagenes.Add(nameFile);
        string url = "https://firebasestorage.googleapis.com/v0/b/friendly-art-304619.appspot.com/o/" + nameFile + "?alt=media";
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(url);

        yield return request.SendWebRequest();
       
        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
        } else
        {   
            Texture2D img = DownloadHandlerTexture.GetContent(request);
            img.Apply();
            image.sprite = Sprite.Create(img, new Rect(0, 0, img.width, img.height), new Vector2(0.5f, 0.5f));
        }
    }

    private void setUIVideo(string nameFile) 
    {
        videos.Add(nameFile);

        var video = GameObject.Find("VideoPlayer").GetComponent<UnityEngine.Video.VideoPlayer>();
        string url = "https://firebasestorage.googleapis.com/v0/b/friendly-art-304619.appspot.com/o/" + nameFile + "?alt=media";
        
        video.url = url;
        video.playOnAwake = false;
    }

    private IEnumerator setAudioClip(string nameFile)
    {
        audios.Add(nameFile);
        string url = "https://firebasestorage.googleapis.com/v0/b/friendly-art-304619.appspot.com/o/" + nameFile + "?alt=media";

        UnityWebRequest www = UnityWebRequestMultimedia.GetAudioClip(url, AudioType.MPEG);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            AudioClip clip = DownloadHandlerAudioClip.GetContent(www);
            var audio = GameObject.Find("Audio").GetComponent<UnityEngine.AudioSource>();
            audio.clip = clip;
            audio.playOnAwake = false;
        }
    }

    public List<string> getVideos()
    {
        return this.videos;
    }

    public List<string> getImagenes()
    {
        return this.imagenes;
    }

    public List<string> getAudios()
    {
        return this.audios;
    }
}
