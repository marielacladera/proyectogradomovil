﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using Newtonsoft.Json;

public class ListCuriousQuestion : MonoBehaviour
{
    public List<string> questions = new List<string>();
    private Root response;
    public string questionId = "";

    void Start()
    {
        listQuestions();
    }

    public void listQuestions()
    {
        StartCoroutine(getCourritine());
    }

    private IEnumerator getCourritine()
    {
        UnityWebRequest request = UnityWebRequest.Get("https://firestore.googleapis.com/v1/projects/friendly-art-304619/databases/(default)/documents/curious_questions");
        yield return request.SendWebRequest();
        if(request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
        }
        else
        {
            GameObject pregunta = GameObject.Find("Pregunta");
            
            var bodyString = request.downloadHandler.text;

            response = JsonConvert.DeserializeObject<Root>(bodyString);
            for (int i = 0; i < response.documents.Count; i++)
            {
                string ruta = response.documents[i].name;
                string question = response.documents[i].fields.curious_question.stringValue;
                questionId = response.documents[i].name;

                GameObject duplicate = Instantiate(pregunta);
                duplicate.transform.SetParent(GameObject.Find("GameObject").transform);
                duplicate.transform.position = pregunta.transform.position;
                duplicate.transform.localScale = pregunta.transform.localScale;
                duplicate.transform.position = new Vector3(duplicate.transform.position.x, duplicate.transform.position.y - 60F * i, duplicate.transform.position.z);
                duplicate.GetComponent<Text>().text = question;
                GameObject children = duplicate.transform.GetChild(0).gameObject;
                children.GetComponent<Text>().text = questionId;
            }
        }
    }

}
