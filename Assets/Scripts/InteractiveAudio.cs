﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

public class InteractiveAudio : MonoBehaviour
{
    public SimpleCloudRecoEventHandler instance;
    public int contador = 0;
    public GameObject audioSource;

    public void atras()
    {
        List<string> lista = instance.getAudios();
        int listSize = lista.Count;

        if (contador > 0)
        {
            StartCoroutine(setAudioClip(lista[contador]));
            contador = contador - 1;
        }
    }

    public void adelante()
    {
        List<string> lista = instance.getAudios();
        int listSize = lista.Count;

        if (contador < listSize - 1)
        {
            StartCoroutine(setAudioClip(lista[contador]));
            contador = contador + 1;
        }
    }

    private IEnumerator setAudioClip(string nameFile)
    {
        string url = "https://firebasestorage.googleapis.com/v0/b/friendly-art-304619.appspot.com/o/" + nameFile + "?alt=media";
        UnityWebRequest www = UnityWebRequestMultimedia.GetAudioClip(url, AudioType.MPEG);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            AudioClip clip = DownloadHandlerAudioClip.GetContent(www);
            var audio = audioSource.GetComponent<UnityEngine.AudioSource>();
            audio.clip = clip;
            audio.Play();
        }
    }
}
