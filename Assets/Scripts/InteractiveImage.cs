﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

public class InteractiveImage : MonoBehaviour
{
    public SimpleCloudRecoEventHandler instance;
    public int contador = 0;
    public UnityEngine.UI.Image image;

    public void atras()
    {
        List<string> lista = instance.getImagenes();
        int listSize = lista.Count;

        if (contador > 0)
        {
            StartCoroutine(setUIImage(lista[contador]));
            contador = contador - 1;
        }
    }

    public void adelante()
    {
        List<string> lista = instance.getImagenes();
        int listSize = lista.Count;

        if (contador < listSize - 1)
        {
            StartCoroutine(setUIImage(lista[contador]));
            contador = contador + 1;
        }
    }

    private IEnumerator setUIImage(string nameFile)
    {
        string url = "https://firebasestorage.googleapis.com/v0/b/friendly-art-304619.appspot.com/o/" + nameFile + "?alt=media";
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(url);

        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
        }
        else
        {
            Texture2D img = DownloadHandlerTexture.GetContent(request);
            img.Apply();
            image.sprite = Sprite.Create(img, new Rect(0, 0, img.width, img.height), new Vector2(0.5f, 0.5f));
        }
    }
}

