﻿using System;
using System.Collections.Generic;

public class Weight
{
    public string stringValue { get; set; }
}

public class Value
{
    public string stringValue { get; set; }
}

public class ArrayValue
{
    public List<Value> values { get; set; }
}

public class Files
{
    public ArrayValue arrayValue { get; set; }
}

public class Habitad
{
    public string stringValue { get; set; }
}

public class Gender
{
    public string stringValue { get; set; }
}

public class High
{
    public string stringValue { get; set; }
}

public class CommonName
{
    public string stringValue { get; set; }
}

public class Width
{
    public string stringValue { get; set; }
}

public class IdTarget
{
    public string stringValue { get; set; }
}

public class ScientificName
{
    public string stringValue { get; set; }
}

public class Description
{
    public string stringValue { get; set; }
}

public class Was
{
    public string stringValue { get; set; }
}

public class Fields
{
    public Weight weight { get; set; }
    public Habitad habitad { get; set; }
    public Gender gender { get; set; }
    public High high { get; set; }
    public CommonName common_name { get; set; }
    public Width width { get; set; }
    public Files files { get; set; }
    public IdTarget id_target { get; set; }
    public ScientificName scientific_name { get; set; }
    public Description description { get; set; }
    public Was was { get; set; }
}

public class FirestoreResponse
{
    public string name { get; set; }
    public Fields fields { get; set; }
    public string createTime { get; set; }
    public string updateTime { get; set; }
}

