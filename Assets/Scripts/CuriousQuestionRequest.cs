﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using Newtonsoft.Json;


public class CuriousQuestionRequest : MonoBehaviour
{
    private Document response;
    public List<string> imagenes = new List<string>();
    public UnityEngine.UI.Image image;
    // Start is called before the first frame update
    void Start()
    {
        getQuestion();
    }

    public void getQuestion()
    {
        StartCoroutine(getCourrutine());
    }

    private IEnumerator getCourrutine()
    {
        string questionId = CuriousQuestionController.cuestionId;
        UnityWebRequest request = UnityWebRequest.Get("https://firestore.googleapis.com/v1/" + questionId);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
        }
        else
        {
            var bodyString = request.downloadHandler.text;
            response = JsonConvert.DeserializeObject<Document>(bodyString);
            setUIText();
            setUIFiles();
        }
    }

    private void setUIText()
    {
        Text pregunta = GameObject.Find("Pregunta").GetComponent<Text>();
        Text respuesta = GameObject.Find("Respuesta").GetComponent<Text>();

        respuesta.text = response.fields.answer.stringValue;
        pregunta.text = response.fields.curious_question.stringValue;
    }

    private void setUIFiles()
    {
        setFiles();
    }

    private void setFiles()
    {
        var links = response.fields.files.arrayValue.values;
        for (var i = 0; i < links.Count; i++)
        {
            string file = links[i].stringValue;
            string[] divs = file.Split('.');
            StartCoroutine(setUIImage(links[i].stringValue));
        }
    }

    private IEnumerator setUIImage(string nameFile)
    {
        imagenes.Add(nameFile);
        string url = "https://firebasestorage.googleapis.com/v0/b/friendly-art-304619.appspot.com/o/" + nameFile + "?alt=media";
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(url);
        yield return request.SendWebRequest();
        if(request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
        }
        else
        {
            Texture2D img = DownloadHandlerTexture.GetContent(request);
            img.Apply();
            image.sprite = Sprite.Create(img, new Rect(0, 0, img.width, img.height), new Vector2(0.5f, 0.5f));
        }
    }

    public List<string> getImagenes()
    {
        return this.imagenes;
    }
}
