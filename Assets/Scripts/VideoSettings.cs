﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoSettings : MonoBehaviour
{
    private bool pause = true;
    
    public void settings() 
    {
        var video = GameObject.Find("VideoPlayer").GetComponent<UnityEngine.Video.VideoPlayer>();
        if(pause == true)
        {
            video.Pause();
            video.playOnAwake = false;
            pause = false;
        }
        else {
            video.Play();
            video.playOnAwake = true;
            pause = true;
        }    
    }
}
