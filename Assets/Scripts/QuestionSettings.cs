﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class QuestionSettings : MonoBehaviour
{
    public void LoadQuestion()
    {
        GameObject children = gameObject.transform.GetChild(0).gameObject;
        string questionId = children.GetComponent<Text>().text;
        CuriousQuestionController.cuestionId = questionId;
        SceneManager.LoadScene("PreguntaCuriosa");
    }
}
